package com.plamenti;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		verifyBrandSearchEngine();
//		verifyModelSearchEngine();
//		checkBabiesSection();
//
//		checkStreetStylesSection();
//
//		checkSophisticatedShoesSection();

		// Implement search verification
		// search for brand/model
		// verify that results

	}

	private static void verifyBrandSearchEngine(){

		WebDriver driver = getWebDriver();
		maximizeBrowser(driver);
		String url = "https://zalando.co.uk";
		navigateTo(driver, url);
		acceptCookies(driver);
		Actions action = new Actions(driver);
		WebElement brandButton = driver.findElement(By.xpath("//li[@data-name='Brands ']//span[text()='Brands ']"));
		action.moveToElement(driver.findElement(By.xpath("//span[text()='The North Face']"))).click().perform();
		WebElement brandNumberFinder = new WebDriverWait(driver,8).until(ExpectedConditions.visibilityOfElementLocated(By.id("//button[@type='button']//span[text()='Brand']")));
		boolean brandSearchOptionField =! brandNumberFinder.getText().equals("0");
		System.out.println(String.format("Brand search is performed: %b", brandSearchOptionField));
	}

	private static void verifyModelSearchEngine(){
		WebDriver driver = getWebDriver();
		maximizeBrowser(driver);
		String url = "https://zalando.co.uk";
		navigateTo(driver, url);
		printNumberFoundSearchResults(driver, "New Balance 574");
		manageCookies(driver);
		quiteDriver(driver);
	}

	private static void checkSophisticatedShoesSection() {
		WebDriver driver = getWebDriver();
		maximizeBrowser(driver);
		String url = "https://www.zalando.co.uk";
		navigateTo(driver, url);

		By womenButtonElementFinder = By.xpath("//span[@class = 'z-navicat-header_genderText' and text() = 'Women']");
		clickElement(driver, womenButtonElementFinder);

		By sophisticatedSectionElementFinder = By.xpath("//h2[text() = 'Sophisticated shoes']");
		String sophisticatedSectionElementName = "Sophisticated shoes";
		verifySection(driver, sophisticatedSectionElementFinder, sophisticatedSectionElementName);

		manageCookies(driver);
		quiteDriver(driver);

//		// Arrange
//		SophisticatedSectionPage sophisticatedSectionPage = new SophisticatedSectionPage(driver);
//
//		// Act
//		sophisticatedSectionPage.clickWomenButton();
//
//		//Assert
//		assert sophisticatedSectionPage.sophisticatedSectionIsPresent();
	}

	private static void checkStreetStylesSection() {
		WebDriver driver = getWebDriver();
		maximizeBrowser(driver);
		String url = "https://www.zalando.co.uk";
		navigateTo(driver, url);

		By menButtonElementFinder = By.xpath("//span[@class = 'z-navicat-header_genderText' and text() = 'Men']");
		clickElement(driver, menButtonElementFinder);

		By streetSectionElementFinder = By.xpath("//h3[ text() = 'Street Styles']");
		String sophisticatedSectionElementName = "Street Styles";
		verifySection(driver, streetSectionElementFinder, sophisticatedSectionElementName);

		manageCookies(driver);
		quiteDriver(driver);
	}

	private static void checkBabiesSection() {
		WebDriver driver = getWebDriver();
		maximizeBrowser(driver);
		String url = "https://www.zalando.co.uk";
		navigateTo(driver, url);

		By kidsButtonElementFinder = By.xpath("//span[@class='z-navicat-header_genderText' and text()='Kids']");
		clickElement(driver, kidsButtonElementFinder);

		By babySectionElementFinder = By.xpath("//h3[text()='Babies']");
		String babySectionElementName = "Baby section";
		verifySection(driver, babySectionElementFinder, babySectionElementName);

		manageCookies(driver);
		quiteDriver(driver);
	}

	private static WebDriver getWebDriver() {
		System.setProperty("webdriver.chrome.driver", "C:\\Auto\\trash\\chromedriver\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		return driver;
	}
	private static void navigateTo(WebDriver driver, String url) {
		driver.get(url);
	}

	private static void maximizeBrowser(WebDriver driver) {
		driver.manage().window().maximize();
	}

	private static void acceptCookies(WebDriver driver) {
		WebElement acceptCookiesButtonFinder = new WebDriverWait(driver,10).until(ExpectedConditions.visibilityOfElementLocated(By.id("uc-btn-accept-banner")));
		acceptCookiesButtonFinder.click();
	}

	private static void printNumberFoundSearchResults(WebDriver driver, String searchString) {
		WebElement searchFieldElementFinder01 = driver.findElement(By.xpath("//div[@class='z-navicat-header_searchContainer']"));
		searchFieldElementFinder01.click();
		WebElement searchFieldElementFinder02 = new WebDriverWait(driver, 8).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class = 'z-navicat-header_searchInput']")));
		searchFieldElementFinder02.sendKeys((searchString + Keys.ENTER));
		int countFoundSearchResults = driver.findElements(By.xpath("//span[@class ='u-6V88 ka2E9k uMhVZi FxZV-M uc9Eq5 pVrzNP ZkIJC- r9BRio qXofat EKabf7']")).size();
		System.out.println(String.format("Found results are: %d", countFoundSearchResults));
	}

	private static void manageCookies(WebDriver driver) {
		// possible future functionality
		// write cookies to DB
		// export cookies to csv file
		driver.manage().deleteAllCookies();
	}

	private static void quiteDriver(WebDriver driver) {
		// get all console log errors and put them in the DB
		driver.close();
		driver.quit();
	}

	private static void clickElement(WebDriver driver, By elementFinder) {
		long timeoutInSeconds = 30;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(elementFinder));

		WebElement kidsButton = driver.findElement(elementFinder);
		kidsButton.click();
	}

	private static void verifySection(WebDriver driver, By sectionElementFinder, String elementToCheck) {
		long timeoutInSeconds = 30;
		WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
		WebElement element =  wait.until(ExpectedConditions.visibilityOfElementLocated(sectionElementFinder));

		boolean elementIsDisplayed = element.isDisplayed();

		System.out.println(String.format("%s is displayed: %b", elementToCheck, elementIsDisplayed));
	}

}